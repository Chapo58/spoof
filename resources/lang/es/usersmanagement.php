<?php

return [

    // Flash Messages
    'createSuccess'     => 'Usuario creado con éxito! ',
    'updateSuccess'     => 'Usuario actualizado con éxito! ',
    'deleteSuccess'     => 'Usuario eliminado con éxito! ',
    'deleteSelfError'   => 'No puedes eliminarte a ti mismo! ',

    // Show User Tab
    'viewProfile'		          => 'Ver Perfil',
    'editUser'			            => 'Editar Usuario',
    'deleteUser'		           => 'Eliminar Usuario',
    'usersBackBtn'           => 'Volver a usuarios',
    'usersPanelTitle'        => 'Información de usuario',
    'labelUserName'          => 'Usuario:',
    'labelEmail'             => 'Email:',
    'labelFirstName'         => 'Nombre:',
    'labelLastName'          => 'Apellido:',
    'labelRole'              => 'Rol:',
    'labelStatus'            => 'Estado:',
    'labelAccessLevel'       => 'Acceso',
    'labelPermissions'       => 'Pemisos:',
    'labelCreatedAt'         => 'Creado El:',
    'labelUpdatedAt'         => 'Actualizado El:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpConfirm'         => 'Confirmation IP:',
    'labelIpSocial'          => 'Socialite Signup IP:',
    'labelIpAdmin'           => 'Admin Signup IP:',
    'labelIpUpdate'          => 'Last Update IP:',
    'labelDeletedAt'	        => 'Eliminado el',
    'labelIpDeleted'	        => 'Deleted IP:',
    'usersDeletedPanelTitle' => 'Información del usuario borrada',
    'usersBackDelBtn'	       => 'Volver a usuarios eliminados',

    'successRestore' 	  => 'Usuario restaurado con éxito.',
    'successDestroy' 	  => 'Registro de usuario destruido con éxito.',
    'errorUserNotFound' => 'Usuario no encontrado.',

    'labelUserLevel'	 => 'Nivel',
    'labelUserLevels'	=> 'Niveles',

];
