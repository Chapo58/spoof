<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email'			=> 'Email',
    'create_user_ph_email'				  => 'Email',
    'create_user_icon_email'			 => 'fa-envelope',

    'create_user_label_username'		=> 'Usuario',
    'create_user_ph_username'			  => 'Usuario',
    'create_user_icon_username'			=> 'fa-user',

    'create_user_label_firstname'		=> 'Nombre',
    'create_user_ph_firstname'			  => 'Nombre',
    'create_user_icon_firstname'		 => 'fa-user',

    'create_user_label_lastname'		=> 'Apellido',
    'create_user_ph_lastname'			  => 'Apellido',
    'create_user_icon_lastname'			=> 'fa-user',

    'create_user_label_password'		=> 'Contraseña',
    'create_user_ph_password'			  => 'Contraseña',
    'create_user_icon_password'			=> 'fa-lock',

    'create_user_label_pw_confirmation'	=> 'Confirmar Contraseña',
    'create_user_ph_pw_confirmation'	   => 'Confirmar Contraseña',
    'create_user_icon_pw_confirmation'	 => 'fa-lock',

    'create_user_label_location'		=> 'Localización del usuario',
    'create_user_ph_location'			  => 'Localización del usuario',
    'create_user_icon_location'			=> 'fa-map-marker',

    'create_user_label_bio'				=> 'Usuario Bio',
    'create_user_ph_bio'				   => 'Usuario Bio',
    'create_user_icon_bio'				 => 'fa-pencil',

    'create_user_label_twitter_username'=> 'Twitter',
    'create_user_ph_twitter_username'	  => 'Twitter',
    'create_user_icon_twitter_username'	=> 'fa-twitter',

    'create_user_label_github_username'	=> 'GitHub',
    'create_user_ph_github_username'	   => 'GitHub',
    'create_user_icon_github_username'	 => 'fa-github',

    'create_user_label_career_title'	=> 'Ocupación',
    'create_user_ph_career_title'		  => 'Ocupación',
    'create_user_icon_career_title'		=> 'fa-briefcase',

    'create_user_label_education'		=> 'Educación',
    'create_user_ph_education'			  => 'Educación',
    'create_user_icon_education'		 => 'fa-graduation-cap',

    'create_user_label_role'			=> 'Rol del Usuario',
    'create_user_ph_role'				  => 'Rol del Usuario',
    'create_user_icon_role'				=> 'fa-shield',

    'create_user_button_text'			=> 'Crear nuevo usuario',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'				=> 'Editar información del Usuario',

    'label-username'					=> 'Usuario',
    'ph-username'						  => 'Usuario',

    'label-useremail'					=> 'Email',
    'ph-useremail'						  => 'Email',

    'label-userrole_id'					=> 'Nivel de Acceso',
    'option-label'						    => 'Seleccionar Nivel',
    'option-user'						     => 'Usuario',
    'option-editor'						   => 'Editor',
    'option-admin'						    => 'Administrador',
    'submit-btn-text'					  => 'Editar Usuario!',

    'submit-btn-icon'					=> 'fa-save',
    'username-icon'						 => 'fa-user',
    'useremail-icon'					 => 'fa-envelope-o',

];
