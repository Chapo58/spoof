<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modals Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which modals share.
    |
     */

    // CONFIRM SAVE DIALOG
    'confirm_modal_title_text'    => 'Confirmar guardar',
    'confirm_modal_title_std_msg' => 'Por favor confirmar',

    'confirm_modal_title_save_msg'   => 'Por favor confirme sus cambios.',
    'confirm_modal_button_save_text' => 'Guardar',
    'confirm_modal_button_save_icon' => 'save',

    'confirm_modal_button_cancel_text' => 'Cancelar',
    'confirm_modal_button_cancel_icon' => 'fa-close',

    // USER EDIT DIALOG
    'edit_user__modal_text_confirm_message' => '¿Estás seguro de que quieres guardar tus cambios?',
    'edit_user__modal_text_confirm_btn'     => 'Guardar Cambios',

    // DELETE DIALOG
    'confirm_delete_title_text'        => 'Confirmar eliminación de usuario',
    'confirm_modal_button_delete_text' => 'Eliminar',

    // DELETE RESTORE
    'confirm_restore_title_text'        => 'Confirmar restauración del usuario',
    'confirm_modal_button_restore_text' => 'Restaurar',
];
