<?php

return [

    'app'			        => 'Spoof',
    'app2'			       => 'Spoof',
    'home'			       => 'Inicio',
    'login'			      => 'Login',
    'logout'		      => 'Salir',
    'register'		    => 'Registro',
    'resetPword'	   => 'Restablecer Contraseña',
    'toggleNav'		   => 'Toggle Navigation',
    'profile'		     => 'Perfil',
    'editProfile'	  => 'Editar Perfil',
    'createProfile'	=> 'Crear Perfil',
    'dashboard'		   => 'Panel de Control',
    'account' 		    => 'Cuenta',

    'activation'	=> 'Registration Started  | Activation Required',
    'exceeded'		 => 'Error de Activación',

    'editProfile'	  => 'Editar Perfil',
    'createProfile'	=> 'Crear Perfil',
    'adminUserList'	=> 'Usuarios',
    'adminEditUsers'=> 'Editar Usuarios',
    'adminNewUser'	 => 'Agregar Usuario',

    'adminThemesList' => 'Temas',
    'adminThemesAdd'  => 'Agregar Nuevo Tema',

    'adminLogs'		 => 'Log Files',
    'adminPHP'		  => 'PHP Info',
    'adminRoutes'	=> 'Routing Info',

];
