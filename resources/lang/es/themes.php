<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Themes Management Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // Messages
    'createSuccess'     => 'Tema creado! ',
    'updateSuccess'     => 'Tema actualizado! ',
    'deleteSuccess'     => 'Tema eliminado! ',
    'deleteSelfError'   => 'No puedes eliminar el tema por defecto. ',

    // Shared
    'statusLabel'       => 'Estado',
    'statusEnabled'     => 'Habilitado',
    'statusDisabled'    => 'Deshabilitado',

    'nameLabel'    	 	  => 'Nombre del Tema *',
    'namePlaceholder'   => 'Ingresar Nombre del Tema',

    'linkLabel'    	 	  => 'Tema CSS Link *',
    'linkPlaceholder'   => 'Ingresa el link de CSS',

    'notesLabel'    	   => 'Notas del Tema',
    'notesPlaceholder'  => 'Ingresa notas del tema',

    'themes'			=> 'Temas',

    // Add Theme
    'btnAddTheme'    	=> 'Agregar Tema',

    // Edit Theme
    'editTitle'    	 	=> 'Edtando Tema:',
    'editSave'    	 	 => 'gUARDAR cAMBIOS',

    // Show Theme
    'showHeadTitle'		   => 'Tema',
    'showTitle'    	 	  => 'Información del Tema',
    'showBackBtn'  	 	  => 'Volver a Temas',
    'showUsers' 	       => 'Usuarios con el Tema:',
    'showStatus'   	 	  => 'Estados',
    'showLink'    	 	   => 'CSS Link',
    'showNotes'    	 	  => 'Notas',
    'showAdded'    	 	  => 'Agregado',
    'showUpdated'    	  => 'Actualizado',
    'confirmDeleteHdr'  => 'Eliminar Tema',
    'confirmDelete'    	=> '¿Estás seguro de que quieres eliminar este tema??',

    // Show Themes
    'themesTitle'		   => 'Mostrando todo',
    'themesStatus'		  => 'Estado',
    'themesUsers'		   => 'Usuarios',
    'themesName'		    => 'Nombre',
    'themesLink'		    => 'CSS Link',
    'themesActions'		 => 'Acciones',
    'themesBtnShow'		 => 'Ver Tema',
    'themesBtnEdit'		 => 'Editar Tema',
    'themesBtnDelete'	=> 'Eliminar Tema',
    'themesBtnEdits'	 => '',

];
