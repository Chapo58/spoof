<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Usuario o Contraseña Incorrecto.',
    'throttle' => 'Demasiados intentos de ingreso. Pof favor vuelve a intentarlo en :seconds segundos.',

    // Titles
    'loginPageTitle'    => 'Ingresar',

    // Activation items
    'sentEmail'         => 'Te hemos enviado un email a :email.',
    'clickInEmail'      => 'Por favor haz clic en el link para activar tu cuenta.',
    'anEmailWasSent'    => 'Un email fue enviado a :email el :date.',
    'clickHereResend'   => 'Haz clic aqui para reenviar el mail.',
    'successActivated'  => 'Tu cuenta ha sido activada exitosamente.',
    'unsuccessful'      => 'Tu cuenta no pudo ser activada; por favor intentalo nuevamente.',
    'notCreated'        => 'Tu cuenta no pudo ser creada; por favor intentalo nuevamente.',
    'tooManyEmails'     => 'Se han enviado demasiados emails de activación a :email. <br />Por favor intentelo nuevamente en <span class="label label-danger">:hours horas</span>.',
    'regThanks'         => 'Gracias por registrarte, ',
    'invalidToken'      => 'Token de activación invalido. ',
    'activationSent'    => 'Mail de activación enviado. ',
    'alreadyActivated'  => 'Ya activado. ',
    'confirmation'      => 'Confirmación enviada',

    // Labels
    'whoops'            => 'UPS!',
    'someProblems'      => 'Error',
    'email'             => 'E-Mail',
    'password'          => 'Contraseña',
    'rememberMe'        => ' Recordarme',
    'login'             => 'Ingresar',
    'logout'            => 'Salir',
    'forgot'            => '¿Contraseña Olvidada?',
    'forgot_message'    => '¿Poblemas con la contraseña?',
    'name'              => 'Usuario',
    'first_name'        => 'Nombre',
    'last_name'         => 'Apellido',
    'confirmPassword'   => 'Confirmar Contraseña',
    'register'          => 'Registrarse',
    'success'           => 'Exito',
    'message'           => 'Ver Mensajes',

    'noticeTitle'       => 'Noticias',

    // Placeholders
    'ph_name'           => 'Usuario',
    'ph_email'          => 'E-mail',
    'ph_firstname'      => 'Nombre',
    'ph_lastname'       => 'Apellido',
    'ph_password'       => 'Contraseña',
    'ph_password_conf'  => 'Confirmar Contraseña',

    // User flash messages
    'sendResetLink'     => 'Enviar link de restauración de contraseña',
    'resetPassword'     => 'Recuperar Contraseña',
    'loggedIn'          => 'Te has logeado!',

    // email links
    'pleaseActivate'    => 'Por favor activa tu cuenta.',
    'clickHereReset'    => 'Haga clic aquí para restaurar tu contraseña: ',
    'clickHereActivate' => 'Haga clic aquí para activar su cuenta: ',

    // Validators
    'userNameTaken'     => 'Usuario actualmente en uso',
    'userNameRequired'  => 'Usuario requerido',
    'fNameRequired'     => 'Nombre requerido',
    'lNameRequired'     => 'Apellido requerido',
    'emailRequired'     => 'Email es requerido',
    'emailInvalid'      => 'Email invalido',
    'passwordRequired'  => 'Contraseña requerida',
    'PasswordMin'       => 'La contraseña debe tener al menos 6 caracteres',
    'PasswordMax'       => 'La contraseña no debe tener mas de 20 caracteres',
    'captchaRequire'    => 'Captcha requerido',
    'CaptchaWrong'      => 'Captcha incorrecto, por favor intentelo nuevamente.',
    'roleRequired'      => 'Rol de usuario es requerido.',

    'emailLoginError'   => 'Por favor ingresa una dirección de email valida.',
    'pwLoginError'      => 'Por favor ingresa tu contraseña.',

];
