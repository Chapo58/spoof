<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
     */

    'templateTitle' => 'Editar tu perfil',

    // profile erros
    'notYourProfile'      => 'Este no es el perfil que estas buscando.',
    'notYourProfileTitle' => 'Hmmm, parece que algo mal no salio bien...',
    'noProfileYet'        => 'No hay perfil todavía.',

    // USER profile title
    'showProfileUsername'        => 'Usuario',
    'showProfileFirstName'       => 'Nombre',
    'showProfileLastName'        => 'Apellido',
    'showProfileEmail'           => 'E-mail',
    'showProfileLocation'        => 'Localización',
    'showProfileBio'             => 'Bio',
    'showProfileTheme'           => 'Tema',
    'showProfileTwitterUsername' => 'Twitter',
    'showProfileGitHubUsername'  => 'Github',

    // USER profile page
    'showProfileTitle' => 'Perfil de :username ',

    // USER EDIT profile page
    'editProfileTitle' => 'Configuración de Perfil',

    // User edit profile form
    'label-theme' => 'Tu theme:',
    'ph-theme'    => 'Selecciona tu tema',

    'label-location' => 'Tu location:',
    'ph-location'    => 'Ingresa tu localización',

    'label-bio' => 'Tu bio:',
    'ph-bio'    => 'Ingresa tu bio',

    'label-github_username' => 'Usuario de GitHub:',
    'ph-github_username'    => 'Ingresa tu usuario de GitHub',

    'label-twitter_username' => 'Usuario de Twitter:',
    'ph-twitter_username'    => 'Ingresa tu usuario de Twitter',

    // User Account Settings Tab
    'editTriggerAlt'        => 'Toggle User Menu',
    'editAccountTitle'      => 'Configuraciones de la cuenta',
    'editAccountAdminTitle' => 'Administrar Cuenta',
    'updateAccountSuccess'  => 'Tu cuenta ha sido actualizada exitosamente',
    'submitProfileButton'   => 'Guardar Cambios',

    // User Account Admin Tab
    'submitPWButton'    => 'Actualizar Contraseña',
    'changePwTitle'     => 'Cambiar Contraseña',
    'changePwPill'      => 'Cambiar Contraseña',
    'deleteAccountPill' => 'Eliminar Cuenta',
    'updatePWSuccess'   => 'Su contraseña ha sido actualizada satisfactoriamente',

    // Delete Account Tab
    'deleteAccountTitle'        => 'Eliminar mi cuenta',
    'deleteAccountBtn'          => 'Eliminar mi cuenta',
    'deleteAccountBtnConfirm'   => 'Eliminar mi cuenta',
    'deleteAccountConfirmTitle' => 'Confirmar eliminación de cuenta',
    'deleteAccountConfirmMsg'   => '¿Estás seguro de que quieres eliminar tu cuenta?',
    'confirmDeleteRequired'     => 'Se requiere confirmar la eliminación de la cuenta',

    'errorDeleteNotYour'        => 'Solo puedes borrar tu propio perfil.',
    'successUserAccountDeleted' => 'Tu cuenta ha sido eliminada',

    // Messages
    'updateSuccess' => 'Tú perfil ha sido actualizado satisfactoriamente',
    'submitButton'  => 'Guardar Cambios',

    // Restore User Account
    'errorRestoreUserTime' => 'Lo sentimos, la cuenta no puede ser restaurada',
    'successUserRestore'   => 'Bienvenido de vuelta :username! Tu cuenta ha sido exitosamente restaurada!',

    // Save button
    'submitButton'        => 'Guardar',
    'submitChangesButton' => 'Guardar Cambios',

    // User Account
    'accountTitle' => 'Configuraciones de cuenta de :username',

];
