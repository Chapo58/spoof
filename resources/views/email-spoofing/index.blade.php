@extends('layouts.dashboard')

@section('template_title')
    Email Spoofing
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Email Spoofing
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                Email Spoofing
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection

@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                {{$mails->count()}} Mails Enviados
            </h2>

        </div>
        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">
            <div class="table-responsive material-table">
                <table id="user_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Asunto</th>
                        <th class="mdl-data-table__cell--non-numeric">Destino</th>
                        <th class="mdl-data-table__cell--non-numeric">Fecha</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($mails as $mail)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{$mail->asunto}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$mail->destino}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$mail->created_at->format('d/m/Y H:i:s')}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                {!! Form::open(array('url' => 'email-spoofing/' . $mail->id, 'class' => 'inline-block', 'id' => 'delete_'.$mail->id)) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                <a href="#" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$mail->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-userid="{{$mail->id}}">
                                    <i class="material-icons mdl-color-text--red">delete</i>
                                </a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mdl-card__menu" style="top: 15px;">
            <a href="{{ url('/email-spoofing/create') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Enviar Nuevo Mail">
                <i class="material-icons">add_circle_outline</i>
                <span class="sr-only">Enviar Nuevo Mail</span>
            </a>
        </div>
    </div>

    @include('dialogs.dialog-delete')

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        @foreach ($mails as $mail)
        mdl_dialog('.dialiog-trigger{{$mail->id}}','','#dialog_delete');
                @endforeach
        var linkid;
        $('.dialiog-trigger-delete').click(function(event) {
            event.preventDefault();
            linkid = $(this).attr('data-userid');
        });
        $('#confirm').click(function(event) {
            $('form#delete_'+linkid).submit();
        });
    </script>
@endsection
