@extends('layouts.dashboard')

@section('template_title')
    Enviar Nuevo Mail
@endsection

@section('header')
    Enviar Nuevo Mail
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="/email-spoofing">
            <span itemprop="name">
                Email Spoofing
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="/email-spoofing/create">
            <span itemprop="name">
                Enviar Nuevo Mail
            </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-new-user" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--green mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">Enviar Nuevo Mail</h2>
                </div>

                {!! Form::model(new App\Models\Email, ['route' => ['email-spoofing.store'], 'class'=>'', 'role' => 'form', 'files' => true]) !!}
                    @include ('email-spoofing.form')
                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection
