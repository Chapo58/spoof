<div class="mdl-card__supporting-text">
    <div class="mdl-grid full-grid padding-0">
        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

            <div class="mdl-grid ">

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('destino') ? 'is-invalid' :'' }}">
                        {!! Form::email('destino', NULL, array('class' => 'mdl-textfield__input')) !!}
                        {!! Form::label('destino', 'Email Destino', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">El destino es requerido</span>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="mdl-card__actions padding-top-0">
    <div class="mdl-grid padding-top-0">
        <div class="mdl-cell mdl-cell--12-col padding-top-0 margin-top-0 margin-left-1-1">
            <span class="save-actions">
                {!! Form::submit('Enviar Mail', array('class' => 'dialog-button-save mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--green mdl-color-text--white mdl-button--raised margin-bottom-1 margin-top-1 margin-top-0-desktop margin-right-1 padding-left-1 padding-right-1 ')) !!}
            </span>
        </div>
    </div>
</div>

<div class="mdl-card__menu mdl-color-text--white">
    <a href="{{ url('/email-spoofing/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Volver">
        <i class="material-icons">reply</i>
        <span class="sr-only">Volver</span>
    </a>
</div>

@include('dialogs.dialog-save')
