@extends('layouts.dashboard')

@section('template_title')
    Geolocalización
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Geolocalización
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/marcadores')}}" class="">
            <span itemprop="name">
                Geolocalización
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                {{$marcador}}
            </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>
@endsection

@section('content')

    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-wide" style="width:100%;" itemscope itemtype="http://schema.org/Person">
                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                            <ul class="demo-list-icon mdl-list">
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>Nombre:</strong> &nbsp; {{ $marcador->nombre }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>Dirección:</strong> &nbsp; {{ $marcador->direccion }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>Latitud:</strong> &nbsp; {{ $marcador->latitud }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>Longitud:</strong> &nbsp; {{ $marcador->longitud }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">event</i>
                                        <strong>Creado El:</strong> &nbsp; {{ $marcador->created_at->format('d/m/Y H:i:s') }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">event</i>
                                        <strong>Ultima Modificación:</strong> &nbsp; {{ $marcador->updated_at->format('d/m/Y H:i:s') }}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mdl-card__actions">
                        <div class="mdl-grid full-grid">
                            <div class="mdl-cell mdl-cell--12-col">
                                <a href="{{ URL::to('marcadores/' . $marcador->id . '/edit') }}" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-shadow--3dp mdl-button--raised mdl-button--primary mdl-color-text--white">
                                    <i class="material-icons padding-right-half-1">edit</i>
                                    Editar Marcador
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

