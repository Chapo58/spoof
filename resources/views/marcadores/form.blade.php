<div class="mdl-card__supporting-text">
    <div class="mdl-grid full-grid padding-0">
        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

            <div class="mdl-grid ">

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('nombre') ? 'is-invalid' :'' }}">
                        {!! Form::text('nombre', NULL, array('class' => 'mdl-textfield__input')) !!}
                        {!! Form::label('nombre', 'Nombre del marcador', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">El nombre es requerido</span>
                    </div>
                </div>

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('direccion') ? 'is-invalid' :'' }}">
                        {!! Form::text('direccion', NULL, array('class' => 'mdl-textfield__input', 'id' => 'pac-input')) !!}
                        {!! Form::label('direccion', 'Dirección', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">La dirección es requerida</span>
                    </div>
                </div>

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('latitud') ? 'is-invalid' :'' }}">
                        {!! Form::text('latitud', NULL, array('class' => 'mdl-textfield__input', 'id' => 'latitude')) !!}
                        {!! Form::label('latitud', 'Latitud', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">La latitud es requerida</span>
                    </div>
                </div>

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('longitud') ? 'is-invalid' :'' }}">
                        {!! Form::text('longitud', NULL, array('class' => 'mdl-textfield__input', 'id' => 'longitude')) !!}
                        {!! Form::label('longitud', 'Longitud', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">La longitud es requerida</span>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<div id ="map"> </div>
<div class="mdl-card__actions padding-top-0">
    <div class="mdl-grid padding-top-0">
        <div class="mdl-cell mdl-cell--12-col padding-top-0 margin-top-0 margin-left-1-1">
            <span class="save-actions">
                {!! Form::submit('Guardar Marcador', array('class' => 'dialog-button-save mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--green mdl-color-text--white mdl-button--raised margin-bottom-1 margin-top-1 margin-top-0-desktop margin-right-1 padding-left-1 padding-right-1 ')) !!}
            </span>
        </div>
    </div>
</div>

<div class="mdl-card__menu mdl-color-text--white">
    <a href="{{ url('/marcadores/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Volver">
        <i class="material-icons">reply</i>
        <span class="sr-only">Volver</span>
    </a>
</div>

@include('dialogs.dialog-save')
