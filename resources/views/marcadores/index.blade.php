@extends('layouts.dashboard')

@section('template_title')
    Geolocalización
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Geolocalización
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                Geolocalización
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection

@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                {{$marcadores->count()}} Marcadores
            </h2>
        </div>
        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">
            <div class="table-responsive material-table">
                <table id="user_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Nombre</th>
                        <th class="mdl-data-table__cell--non-numeric">Dirección</th>
                        <th class="mdl-data-table__cell--non-numeric">Latitud</th>
                        <th class="mdl-data-table__cell--non-numeric">Longitud</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($marcadores as $marcador)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{$marcador->nombre}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$marcador->direccion}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$marcador->latitud}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$marcador->longitud}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                {{-- VIEW USER PROFILE ICON BUTTON --}}
                                <a href="/marcadores/{{$marcador->id}}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Ver Marcador">
                                    <i class="material-icons mdl-color-text--blue">visibility</i>
                                </a>

                                {{-- EDIT USER ICON BUTTON --}}
                                <a href="{{ URL::to('marcadores/' . $marcador->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                    <i class="material-icons mdl-color-text--orange">edit</i>
                                </a>

                                {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                {!! Form::open(array('url' => 'marcadores/' . $marcador->id, 'class' => 'inline-block', 'id' => 'delete_'.$marcador->id)) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                <a href="#" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$marcador->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-marcadorid="{{$marcador->id}}">
                                    <i class="material-icons mdl-color-text--red">delete</i>
                                </a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="mdl-card__menu" style="top: 15px;">
            <div style="color:#fff;"><strong>Url:</strong> <a href="{{url($link->url)}}" target="_blank" style="color:#fff;">{{url($link->url)}}</a>
                <a href="/link/visitas/{{$link->id}}" target="_blank" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Ver Visitas">
                    <i class="material-icons">group</i>
                    <span class="sr-only">Ver Visitas</span>
                </a>
                <a href="/qr/{{$link->id}}" target="_blank" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Codigo QR">
                    <i class="material-icons">developer_mode</i>
                    <span class="sr-only">Codigo QR</span>
                </a>
                <a href="{{ url('/marcadores/create') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Agregar Nuevo Marcador">
                    <i class="material-icons">add_circle_outline</i>
                    <span class="sr-only">Agregar Nuevo Marcador</span>
                </a>
            </div>
        </div>

    </div>





    @include('dialogs.dialog-delete')

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        @foreach ($marcadores as $marcador)
        mdl_dialog('.dialiog-trigger{{$marcador->id}}','','#dialog_delete');
                @endforeach
        var marcadorid;
        $('.dialiog-trigger-delete').click(function(event) {
            event.preventDefault();
            marcadorid = $(this).attr('data-marcadorid');
        });
        $('#confirm').click(function(event) {
            $('form#delete_'+marcadorid).submit();
        });
    </script>
@endsection
