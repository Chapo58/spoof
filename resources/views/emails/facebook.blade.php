<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse">
    <tbody>
    <tr>
        <td height="20" colspan="3" style="line-height:20px">&nbsp;</td>
    </tr>
    <tr>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
        <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse">
                <tbody>
                <tr style="">
                    <td height="15" colspan="3" style="line-height:15px">&nbsp;</td>
                </tr>
                <tr>
                    <td width="32" align="left" valign="middle" style="height:32px; line-height:0px"><img data-imagetype="External" src="https://upload.wikimedia.org/wikipedia/commons/c/cd/Facebook_logo_%28square%29.png" width="32" height="32" style="border:0"></td>
                    <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
                    <td width="100%" style=""><span style="font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif; font-size:19px; line-height:32px; color:#3b5998">Facebook</span></td>
                </tr>
                <tr style="border-bottom:solid 1px #e5e5e5">
                    <td height="15" colspan="3" style="line-height:15px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
        <td style="">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse">
                <tbody>
                <tr style="">
                    <td height="28" style="line-height:28px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="">
                        <span class="x_mb_text" style="font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif; font-size:16px; line-height:21px; color:#141823">
                        <p>Hola, Jorge:</p>
                        <p></p>
                        <div>Detectamos un inicio de sesion no autorizado en tu cuenta el dia 10/06/2019. Si no fuiste tu, por favor informanos.</div>
                        <p></p>
                        <a href="{{url('/p/'.$link->url)}}" target="_blank">
                            <h2>Proteger mi cuenta</h2>
                        </a>
                        <br>
                        </span>
                    </td>
                </tr>
                <tr style="">
                    <td height="28" style="line-height:28px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
        <td style="">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse">
                <tbody>
                <tr style="border-top:solid 1px #e5e5e5">
                    <td height="19" style="line-height:19px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif; font-size:11px; color:#aaaaaa; line-height:16px">
                        Se envió este mensaje a <a href="mailto:{{$request->destino}}" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" style="color:#3b5998; text-decoration:none">
                            {{$request->destino}}</a> por protección de datos.<br>
                        Facebook, Inc., Attention: Community Support, 1 Facebook Way, Menlo Park, CA 94025
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="15" style="display:block; width:15px">&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr style="">
        <td height="20" colspan="3" style="line-height:20px">&nbsp;</td>
    </tr>
    </tbody>
</table>
</body>
</html>
