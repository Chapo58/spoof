<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Codigo QR</title>
</head>
<body>
    
<div class="visible-print text-center">
	<center>
		{!! QrCode::size(500)->generate(url($link->url)); !!}
	</center>
</div>
    
</body>
</html>