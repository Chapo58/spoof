@extends('layouts.dashboard')

@section('template_title')
    Phishing
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Phishing
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/phishing')}}" class="">
            <span itemprop="name">
                Phishing
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                {{$link}}
            </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>
@endsection

@section('content')

    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-wide" style="width:100%;" itemscope itemtype="http://schema.org/Person">
                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--6-col-desktop">
                            <ul class="demo-list-icon mdl-list">
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>Nombre:</strong> &nbsp; {{ $link->nombre }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">link_off</i>
                                        <strong>Url:</strong> &nbsp;
                                        <a href="{{url($link->url)}}" target="_blank">
                                            {{url($link->url)}}
                                        </a>
                                    </div>
                                </li>
                                @if($link->redireccion_url)
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">link</i>
                                        <strong>Redirecciona a:</strong> &nbsp;
                                        <a href="{{$link->redireccion_url}}" target="_blank">
                                            {{$link->redireccion_url}}
                                        </a>
                                    </div>
                                </li>
                                @endif
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">developer_mode</i>
                                        <strong>Codigo QR:</strong> &nbsp; {!! QrCode::size(200)->generate(url($link->url)); !!}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                Visitas
            </h2>
        </div>
        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">
            <div class="table-responsive material-table">
                <table id="user_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">IP</th>
                        <th class="mdl-data-table__cell--non-numeric">Fecha</th>
                        <th class="mdl-data-table__cell--non-numeric">Navegador</th>
                        <th class="mdl-data-table__cell--non-numeric">Plataforma</th>
                        <th class="mdl-data-table__cell--non-numeric">Dispositivo</th>
                        <th class="mdl-data-table__cell--non-numeric">Registros</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($link->visitas as $visita)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->ip_address}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->created_at->format('d/m/Y H:i:s')}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->browser_name}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->platform_name}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->device_family}}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{$visita->registros->count()}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <a href="/link/visita/{{$visita->id}}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Ver Detalles">
                                    <i class="material-icons mdl-color-text--green">visibility</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

