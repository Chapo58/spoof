@extends('layouts.dashboard')

@section('template_title')
    Phishing
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Phishing
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                Phishing
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection

@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                Links Phishing
            </h2>

        </div>
        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">
            <div class="table-responsive material-table">
                <table id="user_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Nombre</th>
                        <th class="mdl-data-table__cell--non-numeric">Url</th>
                        <th class="mdl-data-table__cell--non-numeric">Visitas</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($links as $link)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{$link->nombre}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <a href="{{url($link->url)}}" target="_blank">
                                    {{url($link->url)}}
                                </a>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">{{$link->visitas->count()}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                {{-- VIEW USER PROFILE ICON BUTTON --}}
                                <a href="/phishing/{{$link->id}}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Ver Visitas">
                                    <i class="material-icons mdl-color-text--green">group</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
