@extends('layouts.dashboard')

@section('template_title')
    Capturar Información
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Capturar Información
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/capturas')}}" class="">
            <span itemprop="name">
                Capturar Información
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/capturas/'.$visita->link->id)}}" class="">
            <span itemprop="name">
                {{$visita->link}}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="3" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                {{$visita}}
            </span>
        </a>
        <meta itemprop="position" content="4" />
    </li>
@endsection

@section('content')

    @if($visita->registros->count())
    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-wide" style="width:100%;" itemscope itemtype="http://schema.org/Person">
                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--6-col-desktop">
                            <ul class="demo-list-icon mdl-list">
                                @foreach($visita->registros as $registro)
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">vpn_key</i>
                                        <strong>{{$registro->input_name}}:</strong> &nbsp; {{ $registro->value }}
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($visita->geo_direccion)
        <div style="height:40%;" class="mdl-grid full-grid margin-top-0 padding-0">
            <div style="height:30%;" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
                <div class="mdl-card card-new-user" style="height:30%;width:100%;" itemscope itemtype="http://schema.org/Person">
                    <div style="width:100%;height:100%;" id="map"></div>
                </div>
            </div>
        </div>
    @endif

    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-wide" style="width:100%;" itemscope itemtype="http://schema.org/Person">
                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--6-col-desktop">
                            <ul class="demo-list-icon mdl-list">
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">flag</i>
                                        <strong>IP:</strong> &nbsp; {{ $visita->ip_address }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">event</i>
                                        <strong>Fecha:</strong> &nbsp; {{ $visita->created_at->format('d/m/Y H:i:s') }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">language</i>
                                        <strong>Hostname:</strong> &nbsp; {{ $visita->isp }}
                                    </div>
                                </li>
                                @if($visita->geo_direccion)
                                    <li class="mdl-list__item mdl-typography--font-light">
                                        <div class="mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-icon">room</i>
                                            <strong>Direccíon:</strong> &nbsp; {{ $visita->geo_direccion }}
                                        </div>
                                    </li>
                                @else
                                    <li class="mdl-list__item mdl-typography--font-light">
                                        <div class="mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-icon">room</i>
                                            <strong>Pais:</strong> &nbsp; {{ $visita->geo_country }}
                                        </div>
                                    </li>
                                    <li class="mdl-list__item mdl-typography--font-light">
                                        <div class="mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-icon">room</i>
                                            <strong>Provincia:</strong> &nbsp; {{ $visita->geo_region }}
                                        </div>
                                    </li>
                                    <li class="mdl-list__item mdl-typography--font-light">
                                        <div class="mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-icon">room</i>
                                            <strong>Departamento/Ciudad:</strong> &nbsp; {{ $visita->geo_city }}
                                        </div>
                                    </li>
                                    <li class="mdl-list__item mdl-typography--font-light">
                                        <div class="mdl-list__item-primary-content">
                                            <i class="material-icons mdl-list__item-icon">room</i>
                                            <strong>Codigo Postal:</strong> &nbsp; {{ $visita->geo_postal_code }}
                                        </div>
                                    </li>
                                @endif
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">room</i>
                                        <strong>Latitud/Longitud:</strong> &nbsp; {{ $visita->geo_latitude }} / {{ $visita->geo_longitude }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">web</i>
                                        <strong>Navegador Nombre:</strong> &nbsp; {{ $visita->browser_name }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">web</i>
                                        <strong>Navegador Familia:</strong> &nbsp; {{ $visita->browser_family }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">web</i>
                                        <strong>Navegador Version:</strong> &nbsp; {{ $visita->browser_version }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">web</i>
                                        <strong>Navegador Engine:</strong> &nbsp; {{ $visita->browser_engine }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">settings_applications</i>
                                        <strong>Plataforma Nombre:</strong> &nbsp; {{ $visita->platform_name }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">settings_applications</i>
                                        <strong>Plataforma Familia:</strong> &nbsp; {{ $visita->platform_family }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">settings_applications</i>
                                        <strong>Plataforma Version:</strong> &nbsp; {{ $visita->platform_version }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">important_devices</i>
                                        <strong>Dispositivo Familia:</strong> &nbsp; {{ $visita->device_family }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">important_devices</i>
                                        <strong>Dispositivo Modelo:</strong> &nbsp; {{ $visita->device_model }}
                                    </div>
                                </li>
                                <li class="mdl-list__item mdl-typography--font-light">
                                    <div class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-icon">important_devices</i>
                                        <strong>Dispositivo Grado:</strong> &nbsp; {{ $visita->device_grade }}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5r51-kKvTXu27v3cd9zpye4m5SdPh-P4&callback=initMap">
    </script>
    <script>

        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{$visita->geo_latitude}}, lng: {{$visita->geo_longitude}}},
                zoom: 13,
            });
            var marker = new google.maps.Marker({
                position: {lat: {{$visita->geo_latitude}}, lng: {{$visita->geo_longitude}}},
                map: map,
                title: '{{$visita->geo_direccion}}'
            });
        }


    </script>
@endsection

