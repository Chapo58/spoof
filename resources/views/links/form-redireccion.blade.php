<div class="mdl-card__supporting-text">
    <div class="mdl-grid full-grid padding-0">
        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

            <div class="mdl-grid ">

                <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('nombre') ? 'is-invalid' :'' }}">
                        {!! Form::text('nombre', NULL, array('class' => 'mdl-textfield__input')) !!}
                        {!! Form::label('nombre', 'Nombre del link', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">El nombre es requerido</span>
                    </div>
                </div>

                {{--<div class="mdl-cell mdl-cell--2-col-tablet mdl-cell--3-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        {!! Form::text('ref', url('').'/l/', array('class' => 'mdl-textfield__input', 'disabled')) !!}
                    </div>
                </div>

                <div class="mdl-cell mdl-cell--2-col-tablet mdl-cell--3-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('url') ? 'is-invalid' :'' }}">
                        {!! Form::text('url', (isset($url)) ? $url : NULL, array('class' => 'mdl-textfield__input')) !!}
                        {!! Form::label('url', 'URL', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">La URL es requerida</span>
                    </div>
                </div>--}}

                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        {!! Form::text('redireccion_url', NULL, array('class' => 'mdl-textfield__input')) !!}
                        {!! Form::label('redireccion_url', 'Url a la que se redireccionara', array('class' => 'mdl-textfield__label')); !!}
                    </div>
                </div>

                {{ Form::hidden('formato', (isset($formato)) ? $formato : $link->formato) }}

            </div>
        </div>

    </div>
</div>

<div class="mdl-card__actions padding-top-0">
    <div class="mdl-grid padding-top-0">
        <div class="mdl-cell mdl-cell--12-col padding-top-0 margin-top-0 margin-left-1-1">
            <span class="save-actions">
                {!! Form::submit('Guardar Link', array('class' => 'dialog-button-save mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--green mdl-color-text--white mdl-button--raised margin-bottom-1 margin-top-1 margin-top-0-desktop margin-right-1 padding-left-1 padding-right-1 ')) !!}
            </span>
        </div>
    </div>
</div>

<div class="mdl-card__menu mdl-color-text--white">
    <a href="{{ url('/capturas/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Volver">
        <i class="material-icons">reply</i>
        <span class="sr-only">Volver a Capturas</span>
    </a>
</div>

@include('dialogs.dialog-save')
