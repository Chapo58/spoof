@extends('layouts.dashboard')

@section('template_title')
    Capturar Información
@endsection

@section('template_fastload_css')
@endsection

@section('header')
    Capturar Información
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
        <a itemprop="item" href="" class="">
            <span itemprop="name">
                Capturar Información
            </span>
        </a>
        <meta itemprop="position" content="2" />
    </li>
@endsection

@section('content')

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">
        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                {{$links->count()}} Links Activos
            </h2>

        </div>
        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">
            <div class="table-responsive material-table">
                <table id="user_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Nombre</th>
                        <th class="mdl-data-table__cell--non-numeric">Tipo</th>
                        <th class="mdl-data-table__cell--non-numeric">Url</th>
                        <th class="mdl-data-table__cell--non-numeric">Visitas</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($links as $link)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{$link->nombre}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                @if ($link->formato == Formato::IMAGEN)
                                    @php
                                        $levelIcon        = 'image';
                                        $levelName        = 'Imagen';
                                        $levelBgClass     = 'mdl-color--blue-200';
                                        $leveIconlBgClass = 'mdl-color--blue-500';
                                    @endphp
                                @else
                                    @php
                                        $levelIcon        = 'link';
                                        $levelName        = 'Redirección';
                                        $levelBgClass     = 'mdl-color--orange-200';
                                        $leveIconlBgClass = 'mdl-color--orange-500';
                                    @endphp
                                @endif
                                <span class="mdl-chip mdl-chip--contact {{ $levelBgClass }} mdl-color-text--white md-chip">
                                    <span class="mdl-chip__contact {{ $leveIconlBgClass }} mdl-color-text--white">
                                        <i class="material-icons">{{ $levelIcon }}</i>
                                    </span>
                                    <span class="mdl-chip__text">{{ $levelName }}</span>
                                </span>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <a href="{{url($link->url)}}" target="_blank">
                                    {{url($link->url)}}
                                </a>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">{{$link->visitas->count()}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                {{-- VIEW --}}
                                <a href="/capturas/{{$link->id}}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Ver Visitas">
                                    <i class="material-icons mdl-color-text--blue">visibility</i>
                                </a>

                                {{-- VIEW VISITORS --}}
                                <a href="/link/visitas/{{$link->id}}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Ver Visitas">
                                    <i class="material-icons mdl-color-text--green">group</i>
                                </a>

                                {{-- QR
                                <a href="/qr/{{$link->id}}" target="_blank" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="Codigo QR">
                                    <i class="material-icons mdl-color-text--blue">developer_mode</i>
                                </a>--}}

                                {{-- EDIT --}}
                                <a href="{{ URL::to('capturas/' . $link->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                    <i class="material-icons mdl-color-text--orange">edit</i>
                                </a>

                                {{-- DELETE --}}
                                {!! Form::open(array('url' => 'capturas/' . $link->id, 'class' => 'inline-block', 'id' => 'delete_'.$link->id)) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                <a href="#" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$link->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-userid="{{$link->id}}">
                                    <i class="material-icons mdl-color-text--red">delete</i>
                                </a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mdl-card__menu" style="top: 15px;">

            <a href="{{ url('/links/create/0') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Agregar Link con Imagen">
                <i class="material-icons">add_photo_alternate</i>
                <span class="sr-only">Agregar Link con Imagen</span>
            </a>

            <a href="{{ url('/links/create/1') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Agregar Link con Redireccionamiento">
                <i class="material-icons" aria-hidden="true">link</i>
                <span class="sr-only">Agregar Link con Redireccionamiento</span>
            </a>

        </div>
    </div>

    @include('dialogs.dialog-delete')

@endsection

@section('footer_scripts')
    <script type="text/javascript">
        @foreach ($links as $link)
        mdl_dialog('.dialiog-trigger{{$link->id}}','','#dialog_delete');
                @endforeach
        var linkid;
        $('.dialiog-trigger-delete').click(function(event) {
            event.preventDefault();
            linkid = $(this).attr('data-userid');
        });
        $('#confirm').click(function(event) {
            $('form#delete_'+linkid).submit();
        });
    </script>
@endsection
