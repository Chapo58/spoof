@extends('layouts.dashboard')

@section('template_title')
    Crear Link
@endsection

@section('header')
    Crear Link
@endsection

@section('template_linked_css')
    <link href="{{asset('css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="/capturas">
            <span itemprop="name">
                Capturar Información
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="/capturas/create/{{$formato}}">
            <span itemprop="name">
                Crear Nuevo Link
            </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-new-user" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--green mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">Crear Nuevo Link</h2>
                </div>

                {!! Form::model(new App\Models\Link, ['route' => ['capturas.store'], 'class'=>'', 'role' => 'form', 'files' => true]) !!}
                @if($formato == Formato::IMAGEN)
                    @include ('links.form-imagen')
                @else
                    @include ('links.form-redireccion')
                @endif
                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script src="{{asset('js/dropify.min.js')}}" type="text/javascript"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
