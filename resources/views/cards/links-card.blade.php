<div class="mdl-card mdl-shadow--2dp mdl-cell margin-top-0-tablet-important mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-desktop weather-container">
    <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
        <h2 class="mdl-card__title-text">
            Links Activos
        </h2>
    </div>

    <div class="mdl-card__supporting-text margin-top-0 padding-top-0">
        <br>
        @foreach($links as $link)
            @if ($link->formato == Formato::IMAGEN)
                @php
                    $levelIcon        = 'image';
                    $levelName        = 'Imagen';
                    $levelBgClass     = 'mdl-color--blue-200';
                    $leveIconlBgClass = 'mdl-color--blue-500';
                @endphp
            @else
                @php
                    $levelIcon        = 'link';
                    $levelName        = 'Redirección';
                    $levelBgClass     = 'mdl-color--orange-200';
                    $leveIconlBgClass = 'mdl-color--orange-500';
                @endphp
            @endif

        <p>
            <span class="mdl-chip mdl-chip--contact {{ $levelBgClass }} mdl-color-text--white md-chip">
                <span class="mdl-chip__contact {{ $leveIconlBgClass }} mdl-color-text--white">
                    <i class="material-icons">{{ $levelIcon }}</i>
                </span>
                <span class="mdl-chip__text"><a style="color:#fff;" href="{{url('/l/'.$link->url)}}" target="_blank">{{url('/l/'.$link->url)}}</a></span>
            </span>
        </p>
        @endforeach
    </div>
</div>