<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration{

    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('link_id')->unsigned();
            $table->string('ip_address', 191);
            $table->string('isp', 191)->nullable();
            $table->string('geo_country', 191)->nullable();
            $table->string('geo_region', 191)->nullable();
            $table->string('geo_city', 191)->nullable();
            $table->string('geo_postal_code', 191)->nullable();
            $table->string('geo_latitude', 191)->nullable();
            $table->string('geo_longitude', 191)->nullable();
            $table->string('browser_name', 191)->nullable();
            $table->string('browser_family', 191)->nullable();
            $table->string('browser_version', 191)->nullable();
            $table->string('browser_engine', 191)->nullable();
            $table->string('platform_name', 191)->nullable();
            $table->string('platform_family', 191)->nullable();
            $table->string('platform_version', 191)->nullable();
            $table->string('device_family', 191)->nullable();
            $table->string('device_model', 191)->nullable();
            $table->string('device_grade', 191)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('link_id')->references('id')->on('links');
        });
    }

    public function down(){
        Schema::dropIfExists('visitas');
    }
}
