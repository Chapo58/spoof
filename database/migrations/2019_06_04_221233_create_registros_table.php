<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrosTable extends Migration{

    public function up(){
        Schema::create('registros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('visita_id')->unsigned();
            $table->string('input_name', 191);
            $table->string('value', 191);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('visita_id')->references('id')->on('visitas');
        });
    }

    public function down(){
        Schema::dropIfExists('registros');
    }
}
