<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsEnviadosTable extends Migration{

    public function up()
    {
        Schema::create('mails_enviados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('template')->nullable();
            $table->string('direccion_origen', 191);
            $table->string('nombre_origen', 191);
            $table->string('direccion_destino', 191);
            $table->string('asunto', 191);
            $table->longText('contenido')->nullable();
            $table->tinyInteger('error')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::dropIfExists('mails_enviados');
    }

}
