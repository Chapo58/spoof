<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarcadoresTable extends Migration{

    public function up(){
        Schema::create('marcadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('nombre', 191)->nullable();
            $table->string('direccion', 291);
            $table->string('latitud', 191);
            $table->string('longitud', 191);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down(){
        Schema::dropIfExists('marcadores');
    }
}
