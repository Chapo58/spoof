<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class FormatoLink extends Model{

    const IMAGEN   = 0;
    const REDIRECCION   = 1;
    const FACEBOOK_LOGIN   = 2;
    const PAYPAL   = 3;
    const GMAIL   = 4;
    const GEOLOCALIZACION   = 5;
    const MERCADO_LIBRE   = 6;
    const TWITTER   = 7;

    public static $getArray = [
        self::IMAGEN   => 'Imagen',
        self::REDIRECCION   => 'Redireccion',
        self::FACEBOOK_LOGIN   => 'Facebook Login',
        self::PAYPAL   => 'Paypal',
        self::GMAIL   => 'Gmail',
        self::MERCADO_LIBRE   => 'Mercado Libre',
        self::TWITTER   => 'Twitter',
    ];

}
