<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model{

    use SoftDeletes;

    protected $table = 'mails_enviados';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'template',
        'direccion_origen',
        'nombre_origen',
        'destino',
        'asunto',
        'contenido'
    ];

    protected $dates = ['deleted_at', 'created_at'];

    public function __toString(){
        return (string) $this->asunto;
    }

}
