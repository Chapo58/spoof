<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends Model{

    use SoftDeletes;

    protected $table = 'links';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'formato',
        'nombre',
        'url',
        'redireccion_url',
        'imagen_url'
    ];

    protected $dates = ['deleted_at', 'created_at'];

    public function user(){
      return $this->belongsTo(User::class);
    }

    public function visitas(){
        return $this->hasMany(Visita::class)->orderBy('id','DESC');
    }

    public function formatoString(){
        if(isset($this->formato)){
            return Formato::$getArray[$this->formato];
        }else{
            return '';
        }
    }

    public function __toString(){
        return (string) $this->nombre;
    }

}
