<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marcador extends Model{

    use SoftDeletes;

    protected $table = 'marcadores';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'nombre',
        'latitud',
        'longitud',
        'direccion'
    ];

    protected $dates = ['deleted_at', 'created_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function __toString(){
        return (string) $this->nombre;
    }

}
