<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registro extends Model{

    use SoftDeletes;

    protected $table = 'registros';

    protected $primaryKey = 'id';

    protected $fillable = [
        'visita_id',
        'input_name',
        'value'
    ];

    protected $dates = ['deleted_at'];

    public function visita(){
      return $this->belongsTo(Visita::class);
    }

    public function __toString(){
        return (string) $this->value;
    }

}
