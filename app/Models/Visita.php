<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visita extends Model{

    use SoftDeletes;

    protected $table = 'visitas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'link_id',
        'ip_address',
        'isp',
        'geo_country',
        'geo_region',
        'geo_city',
        'geo_postal_code',
        'geo_direccion',
        'geo_latitude',
        'geo_longitude',
        'browser_name',
        'browser_family',
        'browser_version',
        'browser_engine',
        'platform_name',
        'platform_family',
        'platform_version',
        'device_family',
        'device_model',
        'device_grade'
    ];

    protected $dates = ['deleted_at'];

    public function link(){
      return $this->belongsTo(Link::class);
    }

    public function registros(){
        return $this->hasMany(Registro::class);
    }

    public function __toString(){
        return (string) $this->ip_address;
    }

}
