<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class ImageController extends Controller{

    public function resizeImage(){
        return view('resizeImage');
    }

    public function resizeImagePost(Request $request){ //DEMO
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/thumbnail');
        $img = Image::make($image->getRealPath());
        $img->resize(250, 250, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);

        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);

        $this->postImage->add($input);

        return back()
            ->with('success','Imagen Actualizada Correctamente')
            ->with('imageName',$input['imagename']);
    }

    public function guardarImagen(Request $request,$ancho = NULL, $alto = NULL){
        $image = $request->file('image');

        if(isset($image)){
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads');

            $img = Image::make($image->getRealPath());

            if($ancho && $alto){
                $img->resize($ancho, $alto, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $img->save($destinationPath.'/'.$input['imagename']);

            return '/uploads/'.$input['imagename'];
        }

        return false;
    }

}
