<?php

namespace App\Http\Controllers;

use App\Models\FormatoLink;
use App\Models\Link;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $query = Link::where('user_id', $user->id);
        $query->where(function ($q) {
            $q->where('formato', '=', FormatoLink::IMAGEN)
                ->orWhere('formato', '=', FormatoLink::REDIRECCION);
        });
        $links = $query->get();

        if ($user->isAdmin()) {
            return view('pages.admin.home',compact('links'));
        }

        return view('pages.user.home',compact('links'));
    }
}
