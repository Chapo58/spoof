<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\Marcador;
use App\Models\Visita;
use App\Models\FormatoLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use Browser;

class MarcadorController extends Controller{

    public function index(){
        $usuario = Auth::user();

        $link = Link::where('user_id',Auth::user()->id)
            ->where('formato',FormatoLink::GEOLOCALIZACION)
            ->first();

        if(!$link){
            $linkController = New LinkController();

            $link = New Link();
            $link->user_id = $usuario->id;
            $link->formato = FormatoLink::GEOLOCALIZACION;
            $link->nombre = 'Geolocalización';
            $link->url = $linkController->randomId('m');
            $link->save();
        }

        $query = Marcador::where('user_id', $usuario->id);
        $marcadores = $query->get();

        return view('marcadores.index', compact('marcadores','link'));
    }

    public function create(){
        return view('marcadores.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'latitud' => 'required',
            'longitud' => 'required'
        ]);
        $requestData = $request->all();

        $nuevoMarcador = new Marcador($requestData);
        $nuevoMarcador->user_id = Auth::user()->id;
        $nuevoMarcador->save();

        return redirect('marcadores')->withFlashSuccess('Marcador creado correctamente.');
    }

    public function show($id){
        $marcador = Marcador::findOrFail($id);

        if($marcador->user_id == Auth::user()->id){
            return view('marcadores.show', compact('marcador'));
        }
    }

    public function edit($id){
        $marcador = Marcador::findOrFail($id);

        if($marcador->user_id == Auth::user()->id){
            return view('marcadores.edit', compact('marcador'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'latitud' => 'required',
            'longitud' => 'required'
        ]);

        $marcador = Marcador::findOrFail($id);
        $requestData = $request->all();

        $marcador->update($requestData);

        return redirect('marcadores')->withFlashSuccess('Marcador modificado correctamente.');
    }

    public function destroy($id){
        $marcador = Marcador::findOrFail($id);

        if($marcador->user_id == Auth::user()->id){
            Link::destroy($id);
        }

        return redirect('marcadores')->withFlashSuccess('Marcador eliminado correctamente.');
    }

    public function showMapa(Request $request,$url){
        $link = Link::where('url','/m/'.$url)->first();

        $query = Marcador::where('user_id', $link->user->id);
        $marcadores = $query->get();

        $linkController = New LinkController();
        $visita = $linkController->registrarVisita($request,$link);

        return view('geolocalizacion',compact('marcadores','visita'));
    }

    public function guardarVisita(Request $request){
        $visitante = Visita::findOrFail($request->visitor_id);
        $visitante->geo_direccion = $request->dir;
        $visitante->geo_latitude = $request->lat;
        $visitante->geo_longitude = $request->long;
        $visitante->save();

        $resultado = 'Ok';

        return response()->json($resultado);
    }

}
