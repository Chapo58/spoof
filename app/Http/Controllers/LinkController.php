<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\Visita;
use App\Models\FormatoLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use Browser;

class LinkController extends Controller{

    public function index(){
        $usuario = Auth::user();

        $query = Link::where('user_id', $usuario->id);
        $query->where(function ($q) {
            $q->where('formato', '=', FormatoLink::IMAGEN)
                ->orWhere('formato', '=', FormatoLink::REDIRECCION);
        });
        $links = $query->get();

        return view('links.index', compact('links'));
    }

    public function create($formato){
        return view('links.create', compact('formato'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'nombre' => 'required'
        ]);
        $requestData = $request->all();

        $nuevoLink = new Link($requestData);
        $nuevoLink->user_id = Auth::user()->id;
        $nuevoLink->url = $this->randomId('l');
        if($request->redireccion_url){
            $nuevoLink->redireccion_url = $this->addhttp($request->redireccion_url);
        }

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request);
        if($imagen_url != false){
            $nuevoLink->imagen_url = $imagen_url;
        }

        $nuevoLink->save();

        return redirect('capturas')->withFlashSuccess('Link creado correctamente.');
    }

    public function show($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
            return view('links.show', compact('link'));
        }
    }

    public function showVisitor($id){
        $visita = Visita::findOrFail($id);

        if($visita->link->user_id == Auth::user()->id){
            return view('links.show-visita', compact('visita'));
        }
    }

    public function showVisitors($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
            return view('links.visitas', compact('link'));
        }
    }

    public function edit($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
            return view('links.edit', compact('link'));
        }
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $link = Link::findOrFail($id);
        $requestData = $request->all();

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagen($request);
        if($imagen_url != false){
            $requestData['imagen_url'] = $imagen_url;
        }

        if($request->redireccion_url){
            $requestData['redireccion_url'] = $this->addhttp($request->redireccion_url);
        }

        $link->update($requestData);

        return redirect('capturas')->withFlashSuccess('Link modificado correctamente.');
    }

    public function destroy($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
            Link::destroy($id);
        }

        return redirect('capturas')->withFlashSuccess('Link eliminado correctamente.');
    }

    public function randomId($letra){
        $url = '/'.$letra.'/'.str_random(10);

        $validator = Link::where('url',$url)->first();

        if($validator){
            return $this->randomId($letra);
        }

        return $url;
    }

    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    public function clickLink(Request $request, $url){
        $link = Link::where('url','/l/'.$url)->first();

        if($link){
            $this->registrarVisita($request,$link);
            if($link->formato == FormatoLink::IMAGEN){
                return redirect(url($link->imagen_url));
            } else {
                return Redirect::to($link->redireccion_url);
            }
        } else {
            dd("Link incorrecto");
        }
    }

    public function registrarVisita(Request $request, $link){
        $nuevaVisita = New Visita();
        $nuevaVisita->link_id = $link->id;
        $nuevaVisita->ip_address = $request->getClientIp();;
        $nuevaVisita->isp = $request->ipinfo->hostname;
        $nuevaVisita->geo_country = $request->ipinfo->country_name;
        $nuevaVisita->geo_region = $request->ipinfo->region;
        $nuevaVisita->geo_city = $request->ipinfo->city;
        $nuevaVisita->geo_postal_code = $request->ipinfo->postal;
        $nuevaVisita->geo_latitude = $request->ipinfo->latitude;
        $nuevaVisita->geo_longitude = $request->ipinfo->longitude;
        $nuevaVisita->browser_name = Browser::browserName();
        $nuevaVisita->browser_family = Browser::browserFamily();
        $nuevaVisita->browser_version = Browser::browserVersion();
        $nuevaVisita->browser_engine = Browser::browserEngine();
        $nuevaVisita->platform_name = Browser::platformName();
        $nuevaVisita->platform_family = Browser::platformFamily();
        $nuevaVisita->platform_version = Browser::platformVersion();
        $nuevaVisita->device_family = Browser::deviceFamily();
        $nuevaVisita->device_model = Browser::deviceModel();
        $nuevaVisita->device_grade = Browser::mobileGrade();
        $nuevaVisita->save();

        return $nuevaVisita;
    }

    public function geoLocalizacion(){
        return view('geolocalizacion');
    }

    public function clientInfo(){
        return view('client-information');
    }

    public function qrCode($id){
        $link = Link::findOrFail($id);

        if($link->user_id == Auth::user()->id){
     //       \QrCode::size(500)
     //           ->format('png')
     //           ->generate(url($link->url), public_path('images/qrcode.png'));

            return view('qrCode',compact('link'));
        }

    }

}
